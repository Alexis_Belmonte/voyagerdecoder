#!/usr/bin/env python
#
# coding: utf-8
# 
# Voyager Decoder written in Python 3.8.x
# Copyright 2020 Alexis BELMONTE
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished
# to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
# 

# We import what we need
import pyaudio
import enum
import math

# --- Predeclaration of classes ---
class AudioException:
    pass

class AudioInterface:
    pass

class AudioHostType:
    pass

class AudioHost:
    pass

class AudioDeviceType:
    pass

class AudioDevice:
    pass

class AudioBufferStatusFlags:
    pass

class AudioBufferStatus:
    pass

class AudioStreamFormat:
    pass

class AudioStreamStatus:
    pass

class AudioStreamHandler:
    pass

class AudioStream:
    pass

class AudioStreamMultiplexer:
    pass

class AudioStreamManager:
    pass
# --- Predeclaration of classes ---

# Exception used when an audio or audio-related error occurs
class AudioException(Exception):
    # Constructor for the `AudioException` class with the given `message` object
    def __init__(self, message: str) -> AudioException:
        if not isinstance(message, str):
            raise TypeError("Invalid `message` argument value given")
        
        self.message = message if message else None
    
    # Returns the string message given to the exception's constructor
    def __str__(self) -> str:
        return self.message

# Audio interface that contains the principal functions to obtain & manage devices
class AudioInterface:
    # Constructor for the `AudioInterface` class
    def __init__(self) -> AudioInterface:
        self.__pa_instance = pyaudio.PyAudio()
        
    # Destructor for the `AudioInterface` class`
    def __del__(self):
        self.__pa_instance.terminate()
        self.__pa_instance = None
        
    # Returns the PortAudio instance created by this instance
    def get_portaudio_instance(self) -> pyaudio.PyAudio:
        return self.__pa_instance
        
    # Returns a list of host devices found by PortAudio
    def get_host_list(self) -> list:
        host_list = []
        
        for host_index in range(self.__pa_instance.get_host_api_count()):
            host_info = self.__pa_instance.get_host_api_info_by_index(host_index)
            host_list.append(AudioHost(self, host_info))
            
        return host_list
    
# Enumerator class that list the suppoted host types
@enum.unique
class AudioHostType(enum.Enum):
    IN_DEVELOPMENT          = 0
    DIRECT_SOUND            = 1
    MME                     = 2
    ASIO                    = 3
    SOUND_MANAGER           = 4
    CORE_AUDIO              = 5
    OSS                     = 7
    ALSA                    = 8
    AL                      = 9
    BEOS                    = 10
    WDKMS                   = 11
    JACK                    = 12
    WASAPI                  = 13
    AUDIO_SCIENCE_HPI       = 14
    
    # Function that returns a `str` representation of the enumerator value
    def __str__(self) -> str:
        if   self == AudioHostType.DIRECT_SOUND:
            return "DirectSound"
        elif self == AudioHostType.MME:
            return "MME"
        elif self == AudioHostType.SOUND_MANAGER:
            return "SoundManager"
        elif self == AudioHostType.CORE_AUDIO:
            return "CoreAudio"
        elif self == AudioHostType.OSS:
            return "OSS"
        elif self == AudioHostType.ALSA:
            return "ALSA"
        elif self == AudioHostType.AL:
            return "AL"
        elif self == AudioHostType.BEOS:
            return "BeOS"
        elif self == AudioHostType.WDKMS:
            return "WDKMS"
        elif self == AudioHostType.JACK:
            return "Jack"
        elif self == AudioHostType.WASAPI:
            return "WASAPI"
        elif self == AudioHostType.AUDIO_SCIENCE_HPI:
            return "AudioScience HPI"
        else:
            return "Unknown"
        
# Host class that contains control functions
class AudioHost:    
    # Constructor that takes an `AudioInterface` as it's argument
    def __init__(self, audio_interface: AudioInterface, host_descriptor: dict) -> AudioHost:
        # Value checks
        if   not isinstance(audio_interface, AudioInterface):
            raise TypeError("`audio_interface` argument value is invalid")
        elif not isinstance(host_descriptor, dict):
            raise TypeError("`host_descriptor` argument value is invalid")
        
        self.__audio_interface = audio_interface
        self.__host_descriptor = host_descriptor
        
        # We check the `structVersion` value
        if not   self.__host_descriptor["structVersion"] \
            or   self.__host_descriptor["structVersion"] != 1:
            raise AudioException(
                "`host_descriptor` structure version invalid/unsupported " \
                "while building an `AudioHost`"
            )
        
        # We check if the `index` object is present. NOTE: I use the 
        # `not ... in ...` syntax as `index` may be equal to `0` as the
        # `not self.__host_descrptor["..."]` would not work as expected with
        # this value.
        if not "index" in self.__host_descriptor.keys():
            raise AudioException(
                "Host index is not defined in the host structure while building " \
                "an `AudioHost`"
            )
        if not self.__host_descriptor["type"]:
            raise AudioException(
                "Host name is not defined/invalid in the host structure while " \
                "building an `AudioHost`"
            )
        
        # We create read-only internal variables that will be accessible with
        # getters
        self.__host_index = self.__host_descriptor["index"]
        self.__host_type  = AudioHostType(self.__host_descriptor["type"])
        self.__host_name  =                                                  \
            self.__host_descriptor["name"] if self.__host_descriptor["name"] \
                                         else str(self.__host_type)
        
        # We determine the host device count, the default input device &
        # default output device.
        self.__host_device_count = \
            int(self.__host_descriptor["deviceCount"]) \
                if   self.__host_descriptor["deviceCount"] \
                else 0
            
        self.__host_default_input_device = \
            int(self.__host_descriptor["defaultInputDevice"]) \
                if   (self.__host_descriptor["defaultInputDevice"] and
                      self.__host_descriptor["defaultInputDevice"] != -1) \
                else None
            
        self.__host_default_output_device = \
            int(self.__host_descriptor["defaultOutputDevice"]) \
                if   (self.__host_descriptor["defaultOutputDevice"] and
                      self.__host_descriptor["defaultOutputDevice"] != -1) \
                else None
            
    # Returns the audio interface used to create this object
    def get_audio_interface(self) -> AudioInterface:
        return self.__audio_interface
        
    # Returns the host index/ID
    def get_host_index(self) -> int:
        return self.__host_index
            
    # Returns the host type
    def get_host_type(self) -> AudioHostType:
        return self.__host_type
    
    # Returns the host name
    def get_host_name(self) -> str:
        return self.__host_name
    
    # Returns a list of audio devices
    def get_device_list(self) -> list:
        device_list = []
        
        for device_index in range(self.__audio_interface.get_portaudio_instance().get_device_count()):
            device_list.append(
                AudioDevice(
                    self,
                    self.__audio_interface.get_portaudio_instance().get_device_info_by_host_api_device_index
                    (self.__host_index, device_index)
                )
            )
        
        return device_list
    
    # Returns the default input device
    def get_default_input_device(self) -> AudioDevice:
        return None if self.__host_default_input_device is None \
                    else AudioDevice(
                        self,
                        self.__audio_interface.get_portaudio_instance().get_device_info_by_host_api_device_index
                        (self.__host_index, self.__host_default_input_device)
                    )
    
    # Returns the default output device
    def get_default_output_device(self) -> AudioDevice:
        return None if self.__host_default_output_device is None \
                    else AudioDevice(
                        self,
                        self.__audio_interface.get_portaudio_instance().get_device_info_by_host_api_device_index
                        (self.__host_index, self.__host_default_output_device)
                    )
    
# Type class that enumerates the different device types available
@enum.unique
class AudioDeviceType(enum.Enum):
    INPUT_DEVICE                        = 0b1 << 0
    OUTPUT_DEVICE                       = 0b1 << 1
    BIDIRECTIONAL_DEVICE                = INPUT_DEVICE | OUTPUT_DEVICE
    
    # Returns the type name `str` object from the given `value`
    def __str__(self) -> str:
        if   self == AudioDeviceType.INPUT_DEVICE:
            return "Input"
        elif self == AudioDeviceType.OUTPUT_DEVICE:
            return "Output"
        elif self == AudioDeviceType.BIDIRECTIONAL_DEVICE:
            return "Bidirectional"
        else:
            return "Unknown"
        
# Audio device that contains control functions
class AudioDevice:
    # Constructor that encapsulates the `device_descriptor` structure given by
    # PortAudio and provides functions to interact with the device itself
    def __init__(self, audio_host: AudioHost, device_descriptor: dict) -> AudioDevice:
        if   not isinstance(audio_host, AudioHost):
            raise TypeError("`audio_host` argument value is invalid")
        elif not isinstance(device_descriptor, dict):
            raise TypeError("`device_descriptor` argument value is invalid")
        
        self.__audio_host                   = audio_host
        self.__device_descriptor            = device_descriptor
        
        # We check the `structVersion` field first. If it doesn't exist or
        # the expected value is invalid, we throw an exception.
        if   not self.__device_descriptor["structVersion"] \
              or self.__device_descriptor["structVersion"] != 2:
            raise AudioException(
                "`device_descriptor` structure version invalid/unsupported " \
                "while building an `AudioDevice`"
            )
        # We also check if the `name` field is present, otherwise we throw also
        # an exception.
        elif not self.__device_descriptor["name"]:
            raise AudioException(
                "Device name not defined in the structure while building an " \
                "`AudioDevice`"
            )
        # We finally check if the `index` key is present, otherwise we do the
        # same as above.
        elif not "index" in self.__device_descriptor.keys():
            raise AudioException(
                "Device index not defined in the structure while building an " \
                "`AudioDevice`"
            )
        
        # We copy some fields that doesn't need modifications
        self.__device_name                  = self.__device_descriptor["name"]
        self.__device_index                 = self.__device_descriptor["index"]
        self.__device_input_channel_count   = self.__device_descriptor["maxInputChannels"]
        self.__device_output_channel_count  = self.__device_descriptor["maxOutputChannels"]
        self.__device_default_sample_rate   = self.__device_descriptor["defaultSampleRate"]
        
        # We determine the device type and set the flags accordingly
        __device_type                       = 0
        if   self.__device_input_channel_count  > 0:
            __device_type                  |= AudioDeviceType.INPUT_DEVICE.value
        if self.__device_output_channel_count   > 0:
            __device_type                  |= AudioDeviceType.OUTPUT_DEVICE.value
        self.__device_type                  = AudioDeviceType(__device_type)
        
        # We construct the `device_input_latency` frame
        self.__device_input_latency         = [
            self.__device_descriptor["defaultLowInputLatency"]   if self.__device_descriptor["defaultLowInputLatency"]   != -1 else -math.inf,
            self.__device_descriptor["defaultHighInputLatency"]  if self.__device_descriptor["defaultHighInputLatency"]  != -1 else +math.inf
        ]
            
        # We also construct the `device_output_latency` frame
        self.__device_output_latency        = [
            self.__device_descriptor["defaultLowOutputLatency"]  if self.__device_descriptor["defaultLowOutputLatency"]  != -1 else -math.inf,
            self.__device_descriptor["defaultHighOutputLatency"] if self.__device_descriptor["defaultHighOutputLatency"] != -1 else +math.inf
        ]
            
    # Returns the device host used to create this object
    def get_device_host(self) -> AudioHost:
        return self.__audio_host
    
    # Returns the device name
    def get_device_name(self) -> str:
        return self.__device_name
    
    # Returns the device index
    def get_device_index(self) -> int:
        return self.__device_index
    
    # Returns the input channel count, if applicable
    def get_input_channel_count(self) -> int:
        return self.__device_input_channel_count
    
    # Returns the output channel count, if applicable
    def get_output_channel_count(self) -> int:
        return self.__device_output_channel_count
    
    # Returns the device type
    def get_device_type(self) -> AudioDeviceType:
        return self.__device_type
    
    # Checks if the device accepts an input
    def is_input_device(self) -> bool:
        return self.__device_type.value & AudioDeviceType.INPUT_DEVICE.value > 0
    
    # Checks if the device accepts an output
    def is_output_device(self) -> bool:
        return self.__device_type.value & AudioDeviceType.OUTPUT_DEVICE.value > 0
    
    # Returns the device's default sample rate
    def get_default_sample_rate(self) -> int:
        return self.__device_default_sample_rate
    
    # Returns the device's input latency, if applicable
    def get_input_latency(self) -> float:
        return self.__device_input_latency
    
    # Returns the device's output latency, if applicable
    def get_output_latency(self) -> float:
        return self.__device_output_latency
    
    # Creates an `AudioStream` object with the given arguments
    def create_stream(self, sample_rate: int, channel_count: int,
                      sample_format: AudioStreamFormat,
                      stream_handler: AudioStreamHandler,
                      frames_per_buffer: int = 1024, start_stream: bool = True,
                      enable_input: bool = None, enable_output: bool = None) -> AudioStream:
        # We set default values if the argument variables are not defined
        if enable_input == None:
            enable_input = self.is_input_device()
        if enable_output == None:
            enable_output = self.is_output_device()
        
        # We create an `AudioStream` object
        return AudioStream(
            self, sample_rate, channel_count, sample_format,
            stream_handler, frames_per_buffer, start_stream,
            enable_input, enable_output
        )
    
# Enumeration class that lists the different buffer stream callback flags available
@enum.unique
class AudioBufferStatusFlags(enum.Enum):
    NO_ISSUE            = 0b0
    INPUT_UNDERFLOW     = 0b1 <<  0
    INPUT_OVERFLOW      = 0b1 <<  1
    OUTPUT_UNDERFLOW    = 0b1 <<  2
    OUTPUT_OVERFLOW     = 0b1 <<  3
    PRIMING_OUTPUT      = 0b1 <<  4

# Status class structure that contains information about the audio stream,
# buffer status, etc.
class AudioBufferStatus:
    # Constructor for the `BufferStatus` object
    def __init__(self,
                 audio_stream: AudioStream,
                 buffer_status_flags: AudioBufferStatusFlags,
                 time_information: dict) -> AudioBufferStatus:
        # Value type/value checks
        if   not isinstance(audio_stream, AudioStream):
            raise TypeError("Invalid `audio_stream` argument value")
        elif not isinstance(buffer_status_flags, AudioBufferStatusFlags):
            raise TypeError("Invalid `buffer_status_flags` argument value")
        elif not isinstance(time_information, dict):
            raise TypeError("Invalid `time_information` argument value")
        elif not {"input_buffer_adc_time", "current_time", \
                  "output_buffer_dac_time"} <= set(time_information):
            raise ValueError("Invalid and/or missing `time_information` key(s)")
        
        # We translate the values given from the constructor
        self.__audio_stream         = audio_stream
        self.__buffer_status_flags  = AudioBufferStatusFlags(buffer_status_flags)
        self.__buffer_input_time    = time_information["input_buffer_adc_time"]
        self.__buffer_output_time   = time_information["output_buffer_dac_time"]
        self.__current_time         = time_information["current_time"]
    
    # Returns the audio stream
    def get_audio_stream(self) -> AudioStream:
        return self.__audio_stream
    
    # Returns the buffer status flags
    def get_buffer_status_flags(self) -> AudioBufferStatusFlags:
        return self.__buffer_status_flags
    
    # Returns the buffer input time
    def get_buffer_input_time(self) -> float:
        return self.__buffer_input_time
    
    # Returns the buffer output time
    def get_buffer_output_time(self) -> float:
        return self.__buffer_output_time
    
    # Returns the current time
    def get_current_time(self) -> float:
        return self.__current_time

# Enumeration class that lists the different formats proposed by PortAudio
@enum.unique
class AudioStreamFormat(enum.Enum):
    FLOAT_32            = 0b1 <<  0
    INTEGER_32          = 0b1 <<  1
    INTEGER_24          = 0b1 <<  2
    INTEGER_16          = 0b1 <<  3
    INTEGER_8           = 0b1 <<  4
    UNSIGNED_INTEGER_8  = 0b1 <<  5
    CUSTOM              = 0b1 << 16
    
    # Returns the number of bytes per each sample
    def get_byte_count(self) -> int:
        if   self in (AudioStreamFormat.FLOAT_32, \
                      AudioStreamFormat.INTEGER_32):
            return 4
        elif self ==  AudioStreamFormat.INTEGER_24:
            return 3
        elif self ==  AudioStreamFormat.INTEGER_16:
            return 2
        elif self in (AudioStreamFormat.INTEGER_8, \
                      AudioStreamFormat.UNSIGNED_INTEGER_8):
            return 1
        # We don't know the format, we return 0.
        else:
            return 0
        
    # Returns the numpy type based on the `AudioStreamFormat`. Note that this
    # function may return `None` if the format is not supported.
    def get_numpy_type(self) -> type:
        if   self == AudioStreamFormat.INTEGER_32:
            return numpy.int32
        elif self == AudioStreamFormat.INTEGER_16:
            return numpy.int16
        elif self == AudioStreamFormat.INTEGER_8:
            return numpy.int8
        elif self == AudioStreamFormat.UNSIGNED_INTEGER_8:
            return numpy.uint8
        elif self == AudioStreamFormat.FLOAT_32:
            return numpy.float32
        else:
            return None
        
# Status enumerator that contains the stream control flags
@enum.unique
class AudioStreamStatus(enum.Enum):
    CONTINUE            = 0
    END                 = 1
    ABORT               = 2
    
# Class that contain abstract functions to handle an `AudioStream` object
class AudioStreamHandler:
    # Function callback used when receiving data from the specified device,
    # when applicable.
    def read_data(self, audio_stream: AudioStream, input_data: bytes,
                  frame_count: int, status: AudioBufferStatus):
        pass
    
    # Function callback used when writing data to the specified device, when
    # applicable.
    def write_data(self, audio_stream: AudioStream, frame_count: int,
                   sample_format: AudioStreamFormat,
                   status: AudioBufferStatus) -> bytes:
        pass
    
    # Function callback used to control the status of the assigned stream
    def get_stream_control(self) -> AudioStreamStatus:
        pass

# Class that contains control functions for a bidirectional stream, when
# applicable
class AudioStream:
    # Constructor function for the `AudioStream` class
    def __init__(self, audio_device: AudioDevice, sample_rate: int,
                 channel_count: int, sample_format: AudioStreamFormat,
                 stream_handler: AudioStreamHandler,
                 frames_per_buffer: int = 1024, start_stream: bool = True,
                 enable_input: bool = None, enable_output: bool = None) -> AudioStream:
        # Standard type checks
        if   not isinstance(audio_device, AudioDevice):
            raise TypeError("Invalid `audio_device` argument given")
        elif not isinstance(sample_rate, int):
            raise TypeError("Invalid `sample_rate` argument given")
        elif not isinstance(channel_count, int):
            raise TypeError("Invalid `channel_count` argument given")
        elif not isinstance(sample_format, AudioStreamFormat):
            raise TypeError("Invalid `sample_format` argument given")
        elif not isinstance(stream_handler, AudioStreamHandler):
            raise TypeError("Invalid `stream_handler` argument given")
        elif not isinstance(frames_per_buffer, int):
            raise TypeError("Invalid `frames_per_buffer` argument given")
        elif not isinstance(start_stream, bool):
            raise TypeError("Invalid `start_stream` argument given")
        elif not isinstance(enable_input, bool):
            raise TypeError("Invalid `enable_input` argument given")
        elif not isinstance(enable_output, bool):
            raise TypeError("Invalid `enable_output` argument given")
        
        # Format check
        if   not (sample_format in (AudioStreamFormat.INTEGER_24, \
                                    AudioStreamFormat.CUSTOM)):
            raise AudioException("Unsupported audio stream format \"{:s}\"", str(sample_format))
        
        # We set default values if the argument variables are not defined
        if enable_input  == None:
            enable_input = audio_device.is_input_device()
        if enable_output == None:
            enable_output = audio_device.is_output_device()

        # We define read-only values that will be accessible throughout getter
        # functions.
        self.__stream_handler       = stream_handler
        self.__sample_rate          = sample_rate
        self.__channel_count        = channel_count
        self.__sample_format        = sample_format
        self.__frames_per_buffer    = frames_per_buffer
        self.__input_enabled        = enable_input
        self.__output_enabled       = enable_output
        
        # We get the PortAudio instance from the given audio device
        self.__audio_device         = audio_device
        
        # We create the audio stream
        audio_device_index          = self.__audio_device.get_device_index()
        self.__audio_stream         = pyaudio.PyAudio.open(
            self.__audio_device.get_device_host()        \
                               .get_audio_interface()    \
                               .get_portaudio_instance(),
            sample_rate, channel_count, sample_format.value,
            frames_per_buffer       = frames_per_buffer,
            start                   = start_stream,
            input_device_index      = audio_device_index,
            output_device_index     = audio_device_index,
            input                   = enable_input,
            output                  = enable_output,
            stream_callback         = self.__internal_stream_callback
        )
        
    # Destructor function for the `AudioStream` class
    def __del__(self):
        # If defined, we close & delete the audio stream object
        if hasattr(self, "__audio_stream"):
            self.__audio_stream.stop_stream()
            self.__audio_stream = None
        
    # Internal stream callback used by the PortAudio (pyaudio) API
    def __internal_stream_callback(self, input_data: bytes, frame_count: int,
                                   time_information: dict, status_flags: int) -> tuple:
        # Standard type checks
        if   not isinstance(input_data, bytes) and not (input_data is None):
            raise TypeError("Invalid `input_data` argument given")
        elif not isinstance(frame_count, int):
            raise TypeError("Invalid `frame_count` argument given")
        elif not isinstance(time_information, dict):
            raise TypeError("Invalid `time_information` argument given")
        elif not isinstance(status_flags, int):
            raise TypeError("Invalid `status_flags` argument given")
        
        # We create the buffer status object
        buffer_status = \
            AudioBufferStatus(
                self, AudioBufferStatusFlags(status_flags), time_information
            )
        
        # Calls the input callback depending on the type of device
        if self.__audio_device.is_input_device() and self.__input_enabled:
            self.__stream_handler.read_data(
                self, input_data, frame_count, self.__sample_format,
                buffer_status
            )
        
        # We get the output buffer depending on the type of device
        stream_output_buffer = \
            self.__stream_handler.write_data( \
                self, frame_count, self.__sample_format, buffer_status \
            ) \
            if (self.__audio_device.is_output_device() and self.__output_enabled) else \
               b'\x00' * self.get_expected_buffer_size()
        
        # We assert the returned buffer
        self.assert_buffer(stream_output_buffer)
        
        # We obtain the Control status flag
        stream_control_flag  = self.__stream_handler.get_stream_control()
        # We check if the function has returned the right type, and throw an
        # error if that's the case
        if not (type(stream_control_flag) is AudioStreamStatus):
            raise AudioException(
                "Invalid type returned by stream handler " \
                "extended class (value returned is {:s})".format(
                    type(stream_control_flag).__name__
                )
            )
        
        # We return the tuple expected by PyAudio
        return (stream_output_buffer, stream_control_flag.value)
    
    # Returns the sample rate defined by this stream
    def get_sample_rate(self) -> int:
        return self.__sample_rate
    
    # Returns the channel count defined by this stream
    def get_channel_count(self) -> int:
        return self.__channel_count
    
    # Returns the sample format defined by this stream
    def get_sample_format(self) -> AudioStreamFormat:
        return self.__sample_format
    
    # Returns the frame count per buffer defined by this stream
    def get_frame_count_per_buffer(self) -> int:
        return self.__frames_per_buffer
    
    # Returns the number of bytes expected to be returned by the `write_data`
    # function that the stream expects. If this function fails to determine the
    # sample size in bytes, it will default to a pre-defined value
    # (`sample_size`) and assume it's correctness.
    def get_expected_buffer_size(self, sample_size: int = 4) -> int:
        # Value type check
        if not isinstance(sample_size, int):
            raise TypeError("Invalid `sample_size` argument given")
        
        return (self.__sample_format.get_byte_count() or sample_size) * self.__frames_per_buffer * self.__channel_count
            
    # Returns the input latency of the stream
    def get_input_latency(self) -> float:
        return self.__audio_stream.get_input_latency()
    
    # Returns the output latency of the stream
    def get_output_latency(self) -> float:
        return self.__audio_stream.get_output_latency()
    
    # Returns a `dict` object containing information about the time
    def get_time(self) -> dict:
        return self.__audio_stream.get_time()
    
    # Returns the stream's CPU load occupation
    def get_cpu_load(self) -> float:
        return self.__audio_stream.get_cpu_load()
    
    # Starts the audio stream
    def start_stream(self):
        return self.__audio_stream.start_stream()
    
    # Stops the audio stream
    def stop_stream(self):
        return self.__audio_stream.stop_stream()
    
    # Returns the status of the stream
    def is_active(self) -> bool:
        return self.__audio_stream.is_active() if \
               self.__audio_stream != None   else False
    
    # Gets the number of available frames to be read
    def get_available_read_frames(self) -> bool:
        return self.__audio_stream.get_read_available()
    
    # Gets the number of available frames to be written
    def get_available_write_frames(self) -> bool:
        return self.__audio_stream.get_write_available()
    
    # Asserts the given `stream_buffer` for this stream
    def assert_buffer(stream_buffer: bytes):
        # We get the type & length of the given `stream_buffer` object
        stream_output_buffer_type = type(stream_buffer)
        stream_output_buffer_size = len(stream_buffer)
        expected_buffer_size      = self.get_expected_buffer_size()
        
        # We check if the type & size of the buffer is correct as expected ONLY
        # if we could determine the expected buffer size.
        if   not (isinstance(stream_output_buffer_type, bytes)):
            raise AudioException(
                "Invalid buffer type (expected \"bytes\", got \"{:s}\")".format(
                    stream_output_buffer_type.__name__
                )
            )
        elif expected_buffer_size > 0 and \
             stream_output_buffer_size != expected_buffer_size:
            raise AudioException(
                "Invalid buffer length (expected {:d} bytes, got {:d} " \
                "bytes)".format(
                    expected_buffer_size, stream_output_buffer_size
                )
            )

# Class that allows the use of multiple `AudioStreamHandler` objects for one
# single `AudioStream` instance
class AudioStreamMultiplexer(AudioStreamHandler):
    # Constructor for the `AudioStreamMultiplexer` class
    def __init__(self) -> AudioStreamMultiplexer:
        self.__stream_handler_list = []
        
    # Checks the attached stream handlers' statuses and removes each of them
    # that returns anything that is not equal to `AudioStreamStatus.CONTINUE`
    def __check_stream_handlers(self):
        for stream_handler_index, stream_handler in enumerate(self.__stream_handler_list):
            if stream_handler.get_stream_control() != AudioStreamStatus.CONTINUE:
                self.__stream_handler_list.remove(stream_handler_index)
            
    # Attaches the specified `strema_handler` to the multiplexer
    def attach_handler(self, stream_handler: AudioStreamHandler):
        # Type check
        if not isinstance(stream_handler, AudioStreamHandler):
            raise TypeError("Invalid `stream_handler` argument given")
        
        self.__stream_handler_list.attach(stream_handler)
        
    # Function callback. See the `AudioStreamHandler`'s documentation.
    def read_data(self, audio_stream: AudioStream, input_data: bytes,
                  frame_count: int, status: AudioBufferStatus):
        self.__check_stream_handlers()
        
        for stream_handler in self.__stream_handler_list:
            stream_handler.read_data(
                audio_stream, input_data, frame_count, status
            )
    
    # Function callback. See the `AudioStreamHandler`'s documentation.
    def write_data(self, audio_stream: AudioStream, frame_count: int,
                   sample_format: AudioStreamFormat,
                   status: AudioBufferStatus) -> bytes:
        self.__check_stream_handlers()
        
        stream_handler_buffer_list = []
        stream_handler_buffer_type = audio_stream.get_numpy_type()
        for stream_handler in self.__stream_handler_list:
            stream_handler_buffer = stream_handler.write_data(
                audio_stream, frame_count, sample_format, status
            )
            
            try:
                audio_stream.assert_buffer(stream_handler_buffer)
            except Exception as exception:
                raise AudioException(
                    "In module \"{:s}\": {:s}".format(
                        type(exception).__name__, str(exception)
                    )
                )
                    
            stream_handler_buffer_list.append(
                numpy.frombuffer(
                    stream_handler_buffer,
                    dtype = stream_handler_buffer_type
                )
            )
                    
        return numpy.floor(numpy.mean(stream_handler_buffer_list, axis = 0)) \
                    .astype(stream_handler_buffer_type)                      \
                    .tobytes()
    
    # Function callback. See the `AudioStreamHandler`'s documentation.
    def get_stream_control(self) -> AudioStreamStatus:
        return AudioStreamStatus.CONTINUE if len(self.__stream_handler_list) else \
               AudioStreamStatus.END
        
class AudioStreamManager:
    def __init__(self, audio_stream_handler: AudioStreamHandler,
                 initial_audio_device: AudioDevice, audio_stream_args: dict) -> AudioStreamManager: 
        # We check the given argument values for invalid types
        if   not isinstance(audio_stream_handler, AudioStreamHandler):
            raise TypeError("Invalid `audio_stream_handler` argument given")
        elif not isinstance(initial_audio_device, AudioDevice):
            raise TypeError("Invalid `initial_audio_device` argument given")
        elif not isinstance(audio_stream_args, dict):
            raise TypeError("Invalid `audio_stream_args` argument given")
        
        # We initialize the main fields of the class
        self.__audio_stream_handler      = audio_stream_handler
        self.__active_audio_device       = None
        self.__active_audio_stream       = None
        
        # We save the default values specified
        self.__audio_stream_default_args = audio_stream_args
        
        # We set and configure the given initial audio device
        self.switch_audio_device(initial_audio_device)
    
    # Returns the active audio stream mapped to the selected audio device
    def get_active_audio_stream(self) -> AudioStream:
        return self.__active_audio_stream
        
    # Returns the active audio device that the audio stream is accessing from
    def get_active_audio_device(self) -> AudioDevice:
        return self.__active_audio_device
    
    # Switches to the given audio device and starts the stream automatically
    def switch_audio_device(audio_device: AudioDevice):
        # Type check
        if not isinstance(audio_device, AudioDevice):
            raise TypeError("Invalid `audio_device` argument given")
        
        # We set the active audio device
        self.__active_audio_device = audio_device
        
        # We check if the active audio stream has been defined and is active
        if self.__active_audio_stream and self.__active_audio_stream.is_active():
            # We delete the stream to dispose it correctly
            del self.__active_audio_stream
        
        # We set the new active audio stream to a newly created one by
        # expanding the default arguments specified when constructing the
        # `AudioStreamManager` object to create a stream
        self.__active_audio_stream = self.__active_audio_device.create_stream(
            **self.__audio_stream_default_args
        )
    
