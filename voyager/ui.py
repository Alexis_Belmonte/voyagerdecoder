#!/usr/bin/env python
#
# coding: utf-8
# 
# Voyager Decoder written in Python 3.8.x
# Copyright 2020 Alexis BELMONTE
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished
# to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#

# We import our own modules & the standard modules
import voyager.audio
import os, sys

# We import the GDK, GTK and Pango modules
from gi.repository import Gdk, Gtk, Pango

# --- Predeclaration of classes ---
class MainWindowHandler:
    pass

class MainWindow:
    pass
# --- Predeclaration of classes ---

# Dialog utility class
class DialogUtil:
    # Shows a message of type `type`, with the given `title` & `message` contents
    def show_message(window: Gtk.Window, title: str, message: str, type: Gtk.MessageType):
        message_dialog = Gtk.MessageDialog(
            window,
            0,
            type,
            Gtk.ButtonsType.OK,
            title
        )
        message_dialog.format_secondary_text(message)
        message_dialog.run()
        message_dialog.destroy()
        
# Main window's handler class containing all methods that will be called by
# various actions and hooked up with the help of signals.
class MainWindowHandler:
    # Constructor method that takes a `MainWindow` as it's argument.
    def __init__(self, window_instance: MainWindow) -> MainWindowHandler:
        self.__window_instance = window_instance
    
    # Method called when the window is closed
    def window_main_destroy(self, *args):
        Gtk.main_quit()
        
    # Method called when the Refresh button in the Decoding view is clicked
    def decoding_button_refresh_click(self, *args):
        self.__window_instance.refresh_audio_device_list()
        
    # Method called when the Device Host combobox has changed
    def decoding_combobox_inputhost_changed(self, *args):
        self.__window_instance.refresh_audio_device_list()
        
    # Method called when the Input Host combobx has changed
    def decoding_combobox_inputdevice_changed(self, combobox_inputdevice: Gtk.ComboBox):
        self.__window_instance.set_active_audio_device(
            combobox_inputdevice.get_active()
        )

# Main window class containing all the controls required for the user to
# interact with the program's API.
class MainWindow:
    # --- Widget ID names --- #
    WIDGET_ID_WINDOW_MAIN                       = "window_main"
    
    WIDGET_ID_DECODING_LISTSTORE_INPUTHOST      = "decoding_liststore_inputhost"
    WIDGET_ID_DECODING_LISTSTORE_INPUTDEVICE    = "decoding_liststore_inputdevice"
    
    WIDGET_ID_DECODING_COMBOBOX_INPUTHOST       = "decoding_combobox_inputhost"
    WIDGET_ID_DECODING_COMBOBOX_INPUTDEVICE     = "decoding_combobox_inputdevice"
    # --- Widget ID names --- #
    
    # Constructor method for the `MainWindow` class
    def __init__(self) -> MainWindow:
        try:
            # We build the window using a `Gtk.Builder` object
            self.__window_builder               = Gtk.Builder()
            self.__window_builder.add_from_file(
                os.path.join(voyager.layout_folder, "main_window.glade")
            )
            
            # We create the CSS provider object
            self.__window_css_provider          = Gtk.CssProvider()
            self.__window_css_provider.load_from_path(
                os.path.join(voyager.layout_folder, "main_window.css")
            )
            
            # We add the provider to the window
            Gtk.StyleContext.add_provider_for_screen(
                Gdk.Screen.get_default(),
                self.__window_css_provider,
                Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )
        except Exception as exception:
            DialogUtil.show_message(
                None,
                "An error occured while creating the main window",
                getattr(exception, "message", str(exception)),
                Gtk.MessageType.INFO
            )
            Gtk.main_quit()
            sys.exit(1)
        
        # We get the window created from the `window_builder` object we show
        # an error if this fails.
        self.__window_instance = self.__window_builder.get_object("window_main")
        if self.__window_instance is None:
            DialogUtil.show_message(
                None,
                "An error occured while accessing the main window",
                "Unable to obtain the main window instance. This may probably " +
                "be caused by a modification in the layout file that altered "  +
                "the window name ID.\n Please immediately create a new issue "  +
                "on the repository as soon a possible.\n",
                Gtk.MessageType.INFO
            )
            Gtk.main_quit()
            sys.exit(1)
        
        # We create an `AudioInterface` object (defined in `voyager.audio`, see
        # the module's source-code for more details.
        self.__audio_instance                       = voyager.audio.AudioInterface()
        
        # --- Widget obtention code ---
        self.__widget_decoding_liststore_host       = self.__window_builder.get_object(
            MainWindow.WIDGET_ID_DECODING_LISTSTORE_INPUTHOST
        )
        self.__widget_decoding_liststore_device     = self.__window_builder.get_object(
            MainWindow.WIDGET_ID_DECODING_LISTSTORE_INPUTDEVICE
        )
        
        self.__widget_decoding_combobox_host        = self.__window_builder.get_object(
            MainWindow.WIDGET_ID_DECODING_COMBOBOX_INPUTHOST
        )
        self.__widget_decoding_combobox_device      = self.__window_builder.get_object(
            MainWindow.WIDGET_ID_DECODING_COMBOBOX_INPUTDEVICE
        )
        # --- Widget obtention code ---
        
        # --- Widget initialization code ---
        __widget_crt_host                           = Gtk.CellRendererText()
        __widget_crt_device_type                    = Gtk.CellRendererText()
        __widget_crt_device_type.props.weight_set   = True
        __widget_crt_device_type.props.weight       = Pango.Weight.BOLD
        __widget_crt_device_name                    = Gtk.CellRendererText()
        # --- Widget initialization code ---
        
        # --- Widget configuration code ---
        self.__widget_decoding_combobox_host.pack_start(__widget_crt_host, 1)
        self.__widget_decoding_combobox_host.add_attribute(__widget_crt_host, "text", 0)
        
        self.__widget_decoding_combobox_device.pack_start(__widget_crt_device_type, 1)
        self.__widget_decoding_combobox_device.add_attribute(__widget_crt_device_type, "text", 0)
        self.__widget_decoding_combobox_device.pack_start(__widget_crt_device_name, 2)
        self.__widget_decoding_combobox_device.add_attribute(__widget_crt_device_name, "text", 1)
        # --- Widget configuration code ---
        
        # We refresh the input lists
        self.refresh_audio_host_list()
        self.refresh_audio_device_list()
        
        # We attach the main window event signals to the dedicated class
        self.__window_builder.connect_signals(MainWindowHandler(self))
        
        # We create an `AudioStreamManager` object and set the default input device
        # TODO: self.__audio_stream_manager = 
        
    # Refresh the audio host list
    def refresh_audio_host_list(self):
        self.__widget_decoding_liststore_host.clear()
        
        try:
            # We get the host list and check if it's not empty
            self.__audio_host_list = self.__audio_instance.get_host_list()
            if not len(self.__audio_host_list):
                DialogUtil.show_message(
                    self.__window_instance,
                    "No audio host devices were found",
                    "The PortAudio library could not find any usable host device. "  +
                    "Verify that at least one audio host is available for access.\n" +
                    "On Linux distributions, you may need to install ALSA in order " +
                    "for PortAudio to find an accessible host.",
                    Gtk.MessageType.WARNING
                )
                return
        except voyager.audio.AudioException as exception:
            DialogUtil.show_message(
                self.__window_instance,
                "Unable to get host device list",
                "The `voyager.audio` module interface failed to retrieve a list " +
                "of available host devices.\n" + getattr(exception, 'message', str(exception)),
                Gtk.MessageType.ERROR
            )
            sys.exit(1)
        
        # We add each host found to the list
        for audio_host in self.__audio_host_list:
            self.__widget_decoding_liststore_host.append([
                audio_host.get_host_name()
            ])
            
        # We set the first item active
        self.__widget_decoding_combobox_host.set_active(0)
        
    # Refresh the audio device list
    def refresh_audio_device_list(self):
        self.__widget_decoding_liststore_device.clear()
        
        try:
            # We create an audio device list from the selected host
            self.__audio_device_list = \
                self.__audio_host_list[self.__widget_decoding_combobox_host.get_active()].get_device_list()
        except (voyager.audio.AudioException, OSError) as exception:
            DialogUtil.show_message(
                self.__window_instance,
                "Unable to get audio device list",
                "The `voyager.audio` module interface failed to retrieve a list " +
                "of available audio devices.\n" + getattr(exception, 'message', str(exception)),
                Gtk.MessageType.ERROR
            )
            
            return
            
        # We add each device found to the list
        for audio_device in self.__audio_device_list:
            if audio_device.is_input_device():
                self.__widget_decoding_liststore_device.append([
                    str(audio_device.get_device_type()),
                    audio_device.get_device_name()
                ])
            
        # We set the first item active
        self.__widget_decoding_combobox_device.set_active(0)
        self.set_active_audio_device(0)
        
    # Selects the audio device based on the device index
    def set_active_audio_device(self, device_index: int):
        # Value safety check
        if not (type(device_index) is int) or \
           device_index > len(self.__audio_device_list) or \
           device_index < 0:
            device_index = 0
        
        self.__selected_audio_device = self.__audio_device_list[device_index]
        
    def show(self):
        self.__window_instance.show_all()
        
# Creates a `MainWindow` object and runs the GTK main loop
def run():
    MainWindow().show()
    Gtk.main()
