# Voyager Decoder

## Introduction

The Voyager I spacecraft has been launched in space as a trace of our very own
existence as humans, in addition of being used for the studying of planets;
launched on the 5th of September, 1977. 

The Golden Discs (also called the Golden Records) contains audio on one disc
(such as greetings or sounds of the nature & the wild life) and images stored
as audio on the other disc, as it was impossible to store around 120 images in
the spacecraft's static memory back in 1977. Thus, researchers at NASA decided
that it would be the best to encode their images as audio to fit on the record.

The golden disc that contains images has clues on the cover on how to decode the
waveform, such as the speed that the disc must be played at, or the size of one
image in binary.

This project aims to decode those images with only the information provided on
the back of the disc.

## Research

The following is an image of the indications marked on the back of the Golden Disc,
courtersy of [Wikipedia](https://en.wikipedia.org/wiki/File:The_Sounds_of_Earth_Record_Cover_-_GPN-2000-001978.jpg)

![](/doc/img/readme/golden_disc.jpg)

### First Visual Analysis

The first thing that we can see is at the top left hand corner of the cover is
the position of the stylus & the speed at which the disc must be played at in
binary. We can deduce that dashes signifies `0` and bars signifies `1`. Thus,
we can write the binary number `0b100110000110010000000000000000000` which is
`5113380864` in decimal. This may be probably the speed at which the record
should be played. In any case, we already have the audio file played at the
right speed, so this part of the research was just informational.

The second thing that we can see is two waveforms at at the top right hand
corner of the disc with the numbers `0`, `1` and `2` at the top of each of
them. We can also see another number at the bottom of a period on the first
waveform, indicating `0b101101001100000000000000` in binary, or `11845632` in
decimal. We could interpret this number as the length of the wave in a certain
unit of time, but we have to discover more to deduct it.

At the middle left of the cover, we can see the disc in profile and another
binary number right below: `0b10001011000000000000000000000000000000` which
translates to `149250113536`. Maybe it's the time duration of the Golden Disc?

At the middle right of the cover, we notice two rectangles with the first
probably representing the layout and direction in which the image is shown as
it is drawn.

### First Audio Analysis

We're going to use the "[First Ever Decode of Voyager Audio Images, in Real Time.](https://www.youtube.com/watch?v=ibByF9XPAPg)"
YouTube video. We use the `youtube-dl` utility to download the audio and then
use `ffmpeg` to convert it into the Microsoft WAV format. Finally, we open the
file in Audacity.

If we zoom on a particular section of the file, we get the following image:

![](/doc/img/readme/audacity_audio_analysis.png)

Note that we have split the track into two mono tracks & silenced the right
channel. We have also selected one period for this screenshot. We can notice
that this exactly corresponds to the wave scheme shown on the cover of the
disc.

The period selected allows us to obtain the following frame length:
![](/doc/img/readme/audacity_selected_frame.png)

If we substract the last timestamp with the first timestamp, we get 0.008
seconds (or 8 milliseconds). Thus, we can deduce that each "scanline" in
a single image has a length of 8 milliseconds including the "normalization"
frequency. The "normalization" frequency is selected in this screen capture:

![](/doc/img/readme/audacity_normalization_frequency_selected.png)

We can also see that each image always start with a "beep" produced at around
2 kHz; that way we know when an image's data stream starts.

![](/doc/img/readme/audacity_image_start_selected.png)

An image has a duration of around 4472 milliseconds, as shown here:

![](/doc/img/readme/audacity_image_audio_selected.png)

With the different values acquired, we can calculate the number of scanlines:

![](/doc/img/readme/formula_one.png)

![](/doc/img/readme/formula_two.png)

### First Deductions

We can now deduce the binary numbers present on the Golden Disc's cover.

#### Image Dimensions

![](/doc/img/readme/image_dimensions.png)

The first binary number indicate the number of scanlines (`0b100000000` or
`256`) and the second binary number indicates the number of dots in a single 
scanline (`0b111111110` or `510`). We can thus safely determine that the size
of a single image is 512 dots by 256 scanlines.

#### Period Length

![](/doc/img/readme/image_waveform.png)

The binary number below the waveform indicates the length of a period
(`0b101101001100000000000000` or `11845632` in decimal). The problem is that
this value does not make any sense with the period that we have found manually
(8 milliseconds). I have found a blog post [here](https://boingboing.net/2017/09/05/how-to-decode-the-images-on-th.html)
indicating that the play speed is actually two times faster:

![](/doc/img/readme/formula_three.png)

![](/doc/img/readme/formula_four.png)

The result found is relatively close to what we have found earlier.

### Early Algorithm Proposition

We can propose an algorithm to decode a single image:

- Listen for the 2 kHz tone from a source (file, microphone, or stream)
- Record in a buffer to prevent loses
  - Record a single scanline of 8 milliseconds
  - Increment by one the scanline count 
  - Repeat until 4472 scanlines have been counted
- In parallel: read the scanline list
  - Divide the sample rate of the source by the length of one scan line divided
    by the number of scanlines in an image and divided by two. The result is
    put in a variable named Q
  - For each scanline sample from 0 to ~8000 with an increment of Q as X
    - Plot a pixel of color `-scanline_sampe[X]` at location
      `X % image_width` and `X / image_width`
      
### Technical Solution

For this project, we are going to use Python 3.8 paired with the following
libraries:
- **PyGObject** (`gi`) version 3.36.x, module available [here](https://pypi.org/project/PyGObject/)
- **PortAudio** (`pyaudio`)  version 0.2.x, module documentation available [here](https://people.csail.mit.edu/hubert/pyaudio/#docs).

### License used

This project uses the [MIT license](https://opensource.org/licenses/MIT)
available in both the specified link and [`/LICENSE.md`](/LICENSE.md).

### Icon used

This project uses a modified image of the Voyager's Disc photo that can be
found on [Wikipedia/Wikimedia](https://en.wikipedia.org/wiki/Voyager_Golden_Record#/media/File:The_Sounds_of_Earth_Record_Cover_-_GPN-2000-001978.jpg).

## Sources

- *Voyager 1*, seen on the 21st of May, 2020 on the [Wikipedia website](https://en.wikipedia.org/wiki/Voyager_1).
- *Voyager Golden Record*, seen on the 21st of May, 2020 on the [Wikipedia website](https://en.wikipedia.org/wiki/Voyager_Golden_Record).
- *How to decode the images on the Voyager Golder Record*, seen on the 21st of May, 2020 on the [Boingboing blog](https://boingboing.net/2017/09/05/how-to-decode-the-images-on-th.html).
